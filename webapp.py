"""
The EMPUG website.
2019 03 28 AJL Added links for calendar
"""

from flask import Flask, request, render_template, jsonify

app = Flask(__name__)


# homepage
@app.route("/")
def home_page():
    return render_template("home.html")


# calendar form
@app.route("/calendar")
def calendar():
    return render_template("empug_calendar.html")


# calendar events
@app.route("/data")
def return_data():
    start_date = request.args.get("start", "")
    end_date = request.args.get("end", "")

    with open("events.json", "r") as input_data:
        return input_data.read()


if __name__ == "__main__":
    app.debug = True
    app.run()
