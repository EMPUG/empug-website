# EMPUG Website

### Installation

1. Navigate to a directory somewhere where you keep your software projects:

        cd projects

2. Clone the empug-website repository:

        git clone https://gitlab.com/EMPUG/empug-website.git
        
3. Navigate to the new `empug-website` directory which contains the repository.

        cd empug-website

4. Create a Python 3 virtual environment called `env`:

        python3 -m venv env
        
5. Activate the environment:

        source env/bin/activate
        
6. Install required packages:

        pip install -r requirements.txt


### Running the website

1. Activate the virtual environment, if not already active:

        cd empug-website
        source env/bin/activate
        
2. Launch the Flask application:

        FLASK_APP=webapp.py flask run


### Development

0. Maybe make an issue describing the feature you want to add or the bug you want to fix. 

1. Create a new branch off the latest version of `dev` for your feature:

        git checkout -b feature/my-feature-name

2. Push that up to gitlab and then make a merge request. It might even be a good idea to make a merge request before you start working on the feature, so that you can get feedback on your approach.

        git push origin feature/my-feature-name


## Deploying
Currently we have gitlab runner configured with a staging-deploy and a production-deploy job.
See `.gitlab-ci.yml` for the details.

### Staging
No staging server at the moment, and so no need to use a staging `stg` branch.

### Production
Server: Digital Ocean  
Server IP `167.99.145.104`  
Domain: empug.org  
SSL: Let's Encrypt  
